#!/usr/bin/zsh

anki_server="http://localhost:8765"

deck_name=$(wofi -d -G -p "Front card: " < /dev/null)
if [[ -z "$deck_name" ]]; then
	echo "deck name cannot be empty"
	exit 1
fi

curl -X POST -H 'Content-Type: application/json' \
    -d '{
    "action": "createDeck",
    "version": 6,
    "params": {
        "deck": "'$deck_name'"
    }
}' $anki_server 
