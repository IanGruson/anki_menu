#!/usr/bin/zsh

anki_server="http://localhost:8765"

deck_curl_response=$(
curl -X POST -H 'Content-Type: application/json' \
    -d '{
    "action": "deckNamesAndIds",
    "version": 6
}' $anki_server)

decks=$(echo "$deck_curl_response" | jq .result | jq keys | sed -n 's/.*\"\([^"]\+\)".*/\1/p')

deck_choice=$(echo "$decks" | wofi -d -G)
if [[ -z "$deck_choice" ]]; then
	echo "deck choice cannot be empty"
	exit 1
fi

front_card=$(wofi -d -G -p "Front card: " < /dev/null)
if [[ -z "$front_card" ]]; then
	echo "front card cannot be empty"
	exit 1
fi

back_card=$(wofi -d -G -p "back card: " < /dev/null)
if [[ -z "$back_card" ]]; then
	echo "back card cannot be empty"
	exit 1
fi

curl -X POST -H 'Content-Type: application/json' \
    -d '{
    "action": "addNote",
    "version": 6,
    "params": {
        "note": {
            "deckName": "'$deck_choice'",
            "modelName": "Basic",
            "fields": {
                "Front": "'$front_card'",
                "Back": "'$back_card'"
            },
            "options": {
                "allowDuplicate": false
            }
        }
    }
}' $anki_server 
